# Tooling



## Formatting/Style
- clang-format
- cmake-format


## Benchmarking
- perf
- API/network based things should also be tested "end to end"/"rount trip", i.e., time between packets



# Code



## To read/organise
- [fast int to string conversion](https://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html)
- [incrementing vectors](https://travisdowns.github.io/blog/2019/08/26/vector-inc.html)



## Benchmarking
- https://www.youtube.com/watch?v=2EWejmkKlxs



## General
- don't recalculate immutable results if they can be cached. [ref1]
- use `std::atomic` for thread safety, but prefer to not need it. [ref1]
- use `std::move` to avoid copying, e.g., into constructors. [ref1], [ref3]
- "always do less work". [ref4]



## Branching
- prefer to not need branching. [ref1]
- make branches predictable (random chance of branching in an `if` is bad). [ref1], [ref5] at 40:00



## Loops
- when looping over arrays, traverse in rows (`array[i][j]` instead of `array[j][i]`) [ref5] at 12:00 to 15:00
- try to avoid data dependencies in loops that rely on the previous iteration [ref5] at 50:00



## Variables 
- always use `const`. [ref1]
- prefer const return from switch statements using lambdas. [ref1]
- avoid named temporaries - pass parameters straight into functions etc. [ref1]



## iostream
- avoid `std::endl`; `std::flush` is usually unnecessary  [ref1]
- use `'\n'` as char instead of string



## Strings
- use `std::string`. [ref1]
- initialise `std::string` with value straight away if possible. [ref1]
- avoid long strings when possible (make use of short string optimisation). [ref2]



## Arrays etc.
- prefer `std::array`, then `std::vector`. [ref1]
- don't use others unless you have a very specific reason, and have benchmarked it in your use case. [ref1]
- use `.reserve()` with vectors when possible, _when you know the final size_. [ref4] at 22:40
- - do not use `.reserve()` in a loop, as regular `push_back()` has an amortized constant const (doubles size when there's no free space in the evctor). Calling reserve would only allocate the desired amount, and be done for every loop iteration. [ref7]



## Pointers
- prefer `unique_ptr` to raw pointers for safety. [ref1]
- utilise `std::move` with `unique_ptr` for ownership. [ref4] at 18:50
- don't use rvalue references (`void foo(unique_ptr<int>&& ptr)`) to avoid loading from memory. [ref4] at 22:00
- avoid `shared_ptr` where possible. [ref1]
- avoid passing ` shared_ptr`; take a reference of a type as a function parameter instead. [ref1] at 25:30
- prefer returning `unique_ptr` to `shared_ptr`, unless returning many short-lived objects [ref1] at 38:40 and 44:36



## templates
- avoid as much code as possible inside of templates; make the template as simple as possible. [ref1]



## Prefer Lambdas ("IIFE") [ref1]
- prefer lambdas
- avoid `std::function`
- avoid `std::bind`



## structs
- make related data part of the same struct for data packing. [ref1], [ref5] at 17:40



## final
- use `final` for final implementations of virtual functions (i.e., will not be overriden from a higher class). [ref1]



## abstractions
- use simpler abstractions: [ref6] at 33:00
	- don't use a type when a function would do.
	- don't use a template when a function would do.
	- don't use generated code, when a function/template/meta programming would work.



## Multithreading
- benchmark multithreaded code and align data [ref6] at 44:00 and onwards
	- shared data with modification can cause cache misses (flushed cache) 
- align data to avoid falshe sharing and cache misses due to data being on the same cache line [ref5] at 45:50 and onwards



# References
[ref1]: https://www.youtube.com/watch?v=uzF4u9KgUWI
[ref2]: https://www.youtube.com/watch?v=kPR8h4-qZdk
[ref3]: https://stackoverflow.com/questions/3413470/what-is-stdmove-and-when-should-it-be-used
[ref4]: https://www.youtube.com/watch?v=fHNmRkzxHWs
[ref5]: https://www.youtube.com/watch?v=BP6NxVxDQIs
[ref6]: https://www.youtube.com/watch?v=rHIkrotSwcc
[ref7]: https://www.youtube.com/watch?v=algDLvbl1YY



## Resources (not directly referenced)
- https://github.com/cpp-best-practices/cppbestpractices/blob/master/00-Table_of_Contents.md
- https://www.youtube.com/watch?v=nXaxk27zwlk
- https://www.youtube.com/watch?v=vElZc6zSIXM
- https://www.youtube.com/watch?v=yG1OZ69H_-o
- https://www.youtube.com/watch?v=bSkpMdDe4g4
- https://www.youtube.com/watch?v=SDJImePyftY
- https://www.youtube.com/watch?v=FJJTYQYB1JQ



### not directly code
- https://www.youtube.com/watch?v=D7Sd8A6_fYU
