# dev-tooling

## to-do
- git
- tsconfig
- eslint
- stylelint
- phpcs
- webpack
- - compressing images
- typedoc
- jest
- browsersync
- .env and notes on how to consume
- python venv (notes?)
- cmake basic things
- storybook + lerna (we use this at work and I'm thinking about if it could be useful for me too. Would be nice to have a working TS config with it)
