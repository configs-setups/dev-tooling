import { JsonToJsdocConverter } from "json-to-jsdoc-converter";
import fs from "fs";
const converter = new JsonToJsdocConverter();
// npm run start '{"a": "asdf", "b": 0}'
// Will use this JSON object, unless one was specified in argv as above
// It may be easier to just edit it directly in JS here because of string escaping
const internalJson = JSON.stringify(
	{
		"username": "bob",
		"password": '\n',
	}
);
// process.argv[0] = node
// process.argv[1] = index.js
// process.argv[2] = your json, or leave blank
const inputJson = process.argv[2] || internalJson;
const jsDoc = converter.convert(inputJson);
console.log(jsDoc); // Or you can write to file
fs.writeFile("./output.js", jsDoc, err => err && console.error(err));
